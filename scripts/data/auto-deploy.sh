#!/bin/bash

DIR_TEST="/data/test"
DIR_PROD="/data/prod"
JAR_NAME="centralized-todo-list.jar"
WAIT_TIME=15


wait_test_containers () {
   echo "Wait $WAIT_TIME seconds while the test containers start"
   sleep $WAIT_TIME
   echo "Pending ended"
}
stop_test_containers () {
   docker rm -vf back-test
   docker rm -vf mongo-test
   rm -Rf $DIR_TEST/mongo/logs > /dev/null 2>&1
}


## --  MAIN -- ##
tested=false

# test
mv -f $JAR_NAME $DIR_TEST/$JAR_NAME
cd $DIR_TEST

echo "=> Start test containers"
docker-compose up -d --build
wait_test_containers

./deployment-tests.sh # deployment tests
if [ "$?" = "0" ]; then tested=true; fi

echo "=> Stop test containers"
stop_test_containers

# prod
if [ "$tested" = "true" ]; then
   mv -f $DIR_TEST/$JAR_NAME $DIR_PROD/$JAR_NAME
   cd $DIR_PROD
   echo "=> Start deployment"
   
   echo "Stop previous BACK container"
   docker rm -vf back-prod

   echo "Start new BACK container"
   docker-compose run --name back-prod --service-ports -d back

   echo "=> BACK deployed"
fi

echo "=> Bye"

if [ "$tested" = "false" ]; then exit 99; fi

