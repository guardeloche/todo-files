
//***  JS file for mongo shell  ***//

db.createUser({user: "todo_admin", pwd: "admin", roles: ["userAdminAnyDatabase", "dbAdminAnyDatabase", "readWriteAnyDatabase"]})

db = db.getSiblingDB('todo_db')
db.createCollection("todo")
db.createUser({user: "todo_user", pwd: "todo_pwd", roles: [{role:"readWrite", db:"todo_db"}]})

