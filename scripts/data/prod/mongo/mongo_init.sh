#!/bin/bash

echo "==> Initialization"
mongod --fork --logpath /data/logs/mongodb.log --noauth --smallfiles
mongo admin /mongo/data.js
mongod --shutdown

echo "==> Launch MongoDB"
mongod --httpinterface --rest --noauth --smallfiles

