#!/bin/bash

END_OF_TEST="=> End of deployment tests"
API_ROOT="http://localhost:8080"
API_WORKS="API works"
MAX_HEALTH=5


fatal_step () {
   echo $1
   echo $END_OF_TEST
   exit 99
}
call_api () {
   echo `curl -s -X GET $API_ROOT$1`
}

test_api_works () {
   local count=1
   local resume=true

   local api=`call_api "/api/works" `
   if [ "$api" == "$API_WORKS" ]
      then echo "$API_WORKS"
      else fatal_step "API does not work"
   fi
}
test_api_health () {
   local count=1
   local succeed=0
   local resume=true

   while [ $count -le $MAX_HEALTH ] && [ "$resume" = "true" ]; do
      local health=`call_api "/api/health"`
      local status="failure"

      if [ "$health" = "true" ]
         then
            status="success"
            succeed=$(( $succeed + 1 ))
         else
            resume=false
      fi
      echo "Health check n°$count : $status"

      sleep 1
      count=$(( $count + 1 ))
   done
   
   if [ $succeed -ne $MAX_HEALTH ]
      then
         local ran=$(( $count - 1 ))
         echo "Health checks fails : $succeed succeed / $ran"
         fatal_step "Deployment refused"
      else
         echo "Health checks fully succeeded"
         echo "> Deployment allowed"
   fi
}


##  main  ## 
echo "=> Deployment tests"
test_api_works
test_api_health
echo $END_OF_TEST
